" colorscheme
syntax on
colorscheme Revoltuion

" editor
set tabstop=4
set number

" shortcuts
"	tab switching
map<F1> 001gt
map<F2> 002gt
map<F3> 003gt
map<F4> 004gt
map<F5> 005gt
map<F6> 006gt
map<F7> 007gt
map<F8> 008gt
map<F9> 009gt
map<C-Tab> gt
map<C-S-Tab> gT

